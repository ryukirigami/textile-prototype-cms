import { Customer } from "./customer";

export interface Purchase {
    id: string
    noPq: string
    noPo: string
    warna: string
    jenisBahan: string
    jumlah: string
    harga: string
    status: string
    customer: Customer
}