import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonApiService } from './common-api.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private commonApi: CommonApiService) { }

  getCustomer(params?: any) {
		return this.commonApi.get('customer', params);
	}

  insertCustomer(params: any) {
		return this.commonApi.post('customer', params);
	}

  updateCustomer(params: any) {
		return this.commonApi.put('customer/' + params.id, params);
	}

  deleteCustomer(id: number) {
		return this.commonApi.delete('customer/' + id);
	}
}
