import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonApiService {

  BASE_URL: string;

  constructor(private http: HttpClient) {
    this.BASE_URL = environment.apiServeUrl;
  }

  getHeaders() {
    return new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem("token"));
  }

  get(url: string, params?: any, reqOpts?: any): Observable<any> {

    if (this.checkConnection()) {

      if (!reqOpts) {
        reqOpts = {
          params: new HttpParams()
        };
      }

      if (params) {

        reqOpts.params = new HttpParams();

        for (let reqParams in params) {
          reqOpts.params = reqOpts.params.set(reqParams, params[reqParams]);
        }

      }
      if (localStorage.getItem("token")) {
        reqOpts.headers = this.getHeaders();
      }

      return this.http.get<any>(this.BASE_URL + url, reqOpts)
        .pipe(
          catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              throw error;
            }
          ),
        );

    } else {
      return null as any;
    }

  }

  post(url: string, body: any, reqOpts?: any): Observable<any> {

    if (this.checkConnection()) {

      if (localStorage.getItem("token")) {
        reqOpts = {
          headers: this.getHeaders()
        };
      }

      return this.http.post<any>(this.BASE_URL + url, body, reqOpts)
        .pipe(
          catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              throw error;
            }
          ),
        );

    } else {
      console.log('no internet connected')
      return null as any;
    }

  }

  put(url: string, body: any, reqOpts?: any): Observable<any> {

    if (this.checkConnection()) {

      if (!reqOpts) {
        reqOpts = {
          params: new HttpParams()
        };
      }

      if (localStorage.getItem("token")) {
        reqOpts.headers = this.getHeaders();
      }

      return this.http.put<any>(this.BASE_URL + url, body, reqOpts)
        .pipe(
          catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              throw error;
            }
          ),
        );

    } else {
      console.log('no internet connected')
      return null as any;
    }

  }

  delete(url: string, body?: any): Observable<any> {

    if (this.checkConnection()) {

      let reqOpts: any;

      reqOpts = {
        body: body,
        headers: this.getHeaders()
      }

      return this.http.delete<any>(this.BASE_URL + url, reqOpts)
        .pipe(
          catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              throw error;
            }
          ),
        );

    } else {
      console.log('no internet connected')
      return null as any;
    }

  }

  validate(url: string, body: any, reqOpts?: any): Observable<any> {

    if (this.checkConnection()) {

      const authHeaders = new HttpHeaders({ "Content-Type": "application/x-www-form-urlencoded" })
        .set("Content-Type", "application/x-www-form-urlencoded")
        .set("Authorization", "Basic " + btoa("client:password"));
      reqOpts = { headers: authHeaders };
      let bodyUrl = new URLSearchParams();
      bodyUrl.set("username", body.username);
      bodyUrl.set("grant_type", body.grant_type);
      return this.http.post<any>(this.BASE_URL + url, bodyUrl.toString(), reqOpts)
        .pipe(
          catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              throw error;
            }
          ),
        );

    } else {
      console.log('no internet connected')
      return null as any;
    }

  }

  checkConnection() {
    return navigator.onLine;
  }
}
