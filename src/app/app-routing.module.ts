import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { CustomerComponent } from './menu/master/customer/customer.component';
import { OrderComponent } from './menu/reception/order/order.component';
import { QuotationComponent } from './menu/reception/quotation/quotation.component';
import { TemplatesComponent } from './menu/templates/templates.component';

const routes: Routes = [
  {
		path: '',
		component: AppLayoutComponent,
		children: [
			{
        path: 'customer',
        component: CustomerComponent
      },
      {
        path: 'quotation',
        component: QuotationComponent
      },
      {
        path: 'order',
        component: OrderComponent
      }
      ,
      {
        path: 'templates',
        component: TemplatesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
