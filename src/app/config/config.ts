import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class Config {
	public readonly BASE_URL = 'http://localhost:8090/api/';
}