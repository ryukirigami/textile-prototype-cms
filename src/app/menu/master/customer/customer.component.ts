import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/interfaces/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  customer: Customer = this.resetCustomer();
  customers: Customer[] | undefined;
  inputData: string = '';

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.getData();
    this.resetForm();
  }

  getData(customer?: any) {
    this.customerService.getCustomer().subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.customers = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  insertData(customer: Customer) {
    this.customerService.insertCustomer(customer).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  editData(customer: Customer) {
    console.log(customer);
    this.customer = customer;
  }

  updateData(customer: Customer) {
    this.customerService.updateCustomer(customer).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  deleteData(id: number) {
    this.customerService.deleteCustomer(id).subscribe(
      (response: Customer[]) => {
        console.log(response);
        this.getData();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  resetForm(): void {
    this.customer = this.resetCustomer();
  }

  resetCustomer(): Customer {
    return {
      id: '',
      nama: '',
      alamat: '',
      ktp: '',
      telp: '',
      npwp: ''
    }
  }

}
